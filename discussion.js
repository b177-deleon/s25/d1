db.fruits.insertMany([
    { 
      name: "Apple",
      color: "Red",
      stock: 20,
      price: 40,
      supplier_id: 1,
      onSale: true,
      origin: ["Philippines","US"]
    },
    {
      name: "Banana",
      color: "Yellow",
      stock: 15,
      price: 20,
      supplier_id: 2,
      onSale: true,
      origin: ["Philippines","Ecuador"]
    },
    {
      name: "Kiwi",
      color: "Green",
      stock: 25,
      price: 50,
      supplier_id: 1,
      onSale: true,
      origin: ["US","China"]
    },
    {
      name: "Mango",
      color: "Yellow",
      stock: 10,
      price: 120,
      supplier_id: 2,
      onSale: false,
      origin: ["Philippines","India"] 
    }
]);

// Using the aggregate method
/*
  Syntax:
    db.collection.aggregate([]);
*/

/*
  "$match" method
  Syntax:
    { $match: { field: value } },

  "$group"
  Syntax:
    { $group: { _id: "value", fieldResult: valueResult } }
*/

db.fruits.aggregate([
   { $match: { onSale: true } },
   { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
]);

// field projection with aggregation
/*
  "$project"

*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $project : { _id : 0 } } 
]);

// Sorting aggregated results
/*
  "$sort"
  Syntax:
    { $sort { field: 1/-1 } }
*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $sort: { total: -1 } }
]);

// Aggregate results based on array fields
/*
  "$unwind"
  Syntax:
    { $unwind: field }
*/

db.fruits.aggregate([
  { $unwind: "$origin" }
]);

// Displays fruit documents by their origin and the kinds of fruits that are supplied

db.fruits.aggregate([
  { $unwind: "$origin" },
  { $group: { _id: "$origin", kinds: { $sum: 1 } } },
]);